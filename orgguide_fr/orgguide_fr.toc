\select@language {french}
\contentsline {chapter}{\numberline {1}Introduction}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}Préface}{7}{section.1.1}
\contentsline {section}{\numberline {1.2}Installation}{7}{section.1.2}
\contentsline {section}{\numberline {1.3}Activation}{8}{section.1.3}
\contentsline {section}{\numberline {1.4}Retours}{8}{section.1.4}
\contentsline {chapter}{\numberline {2}Structure du document}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Outlines}{9}{section.2.1}
\contentsline {section}{\numberline {2.2}Lignes d'en-tête}{9}{section.2.2}
\contentsline {section}{\numberline {2.3}Cycle de visibilité}{10}{section.2.3}
\contentsline {section}{\numberline {2.4}Motion}{10}{section.2.4}
\contentsline {section}{\numberline {2.5}Structure d'édition}{11}{section.2.5}
\contentsline {section}{\numberline {2.6}Arbres clairsemés}{11}{section.2.6}
\contentsline {section}{\numberline {2.7}Listes complètes}{12}{section.2.7}
\contentsline {section}{\numberline {2.8}Notes de bas de pages}{13}{section.2.8}
\contentsline {chapter}{\numberline {3}Tableaux}{15}{chapter.3}
\contentsline {chapter}{\numberline {4}Hyperliens}{17}{chapter.4}
\contentsline {section}{\numberline {4.1}Format de lien}{17}{section.4.1}
\contentsline {section}{\numberline {4.2}Liens internes}{17}{section.4.2}
\contentsline {section}{\numberline {4.3}Liens externes}{17}{section.4.3}
\contentsline {section}{\numberline {4.4}Liens manuels}{18}{section.4.4}
\contentsline {section}{\numberline {4.5}Liens ciblés}{19}{section.4.5}
\contentsline {chapter}{\numberline {5}Listes TODO (à faire)}{21}{chapter.5}
\contentsline {section}{\numberline {5.1}Utilisation des états TODO}{21}{section.5.1}
\contentsline {section}{\numberline {5.2}Flux d'états multiples}{22}{section.5.2}
\contentsline {section}{\numberline {5.3}Progrès de journalisation}{23}{section.5.3}
\contentsline {subsubsection}{Fermeture des items}{23}{section*.2}
\contentsline {subsubsection}{Traçage des changements d'états TODO}{23}{section*.3}
\contentsline {section}{\numberline {5.4}Priorités}{23}{section.5.4}
\contentsline {section}{\numberline {5.5}Découper les tâches en sous-tâches}{24}{section.5.5}
\contentsline {section}{\numberline {5.6}Boîtes à cocher}{24}{section.5.6}
\contentsline {chapter}{\numberline {6}Balises}{27}{chapter.6}
\contentsline {section}{\numberline {6.1}Héritage de balise}{27}{section.6.1}
\contentsline {section}{\numberline {6.2}Configuration des balises}{28}{section.6.2}
\contentsline {section}{\numberline {6.3}Balises de groupes}{28}{section.6.3}
\contentsline {section}{\numberline {6.4}Recherche de balise}{29}{section.6.4}
\contentsline {chapter}{\numberline {7}Propriétés}{31}{chapter.7}
\contentsline {chapter}{\numberline {8}Dates et heures}{33}{chapter.8}
\contentsline {section}{\numberline {8.1}Horodateurs}{33}{section.8.1}
\contentsline {section}{\numberline {8.2}Création d'horodatages}{34}{section.8.2}
\contentsline {section}{\numberline {8.3}Dates limites et plannification}{35}{section.8.3}
\contentsline {section}{\numberline {8.4}Chronométrage du temps de travail}{36}{section.8.4}
\contentsline {chapter}{\numberline {9}Capture - Sauvegardes - Archives}{39}{chapter.9}
\contentsline {section}{\numberline {9.1}Capture}{39}{section.9.1}
\contentsline {section}{\numberline {9.2}Configuration d'un emplacement de capture}{39}{section.9.2}
\contentsline {section}{\numberline {9.3}Utilisation de capture}{39}{section.9.3}
\contentsline {section}{\numberline {9.4}Modèles de capture}{40}{section.9.4}
\contentsline {section}{\numberline {9.5}Déposer de nouveau et copier}{41}{section.9.5}
\contentsline {section}{\numberline {9.6}Archivage}{41}{section.9.6}
\contentsline {chapter}{\numberline {10}Vue de l'agenda}{43}{chapter.10}
\contentsline {section}{\numberline {10.1}Fichiers d'agenda}{43}{section.10.1}
\contentsline {section}{\numberline {10.2}Le répartiteur de l'agenda}{43}{section.10.2}
\contentsline {section}{\numberline {10.3}Les vues intégrées dans l'agenda}{44}{section.10.3}
\contentsline {subsubsection}{L'agenda quotidien/hebdomadaire}{44}{section*.4}
\contentsline {subsubsection}{La liste TODO globale}{44}{section*.5}
\contentsline {subsubsection}{Correspondance balises et propriétés}{45}{section*.6}
\contentsline {subsubsection}{Ligne de temps pour un fichier simple}{45}{section*.7}
\contentsline {subsubsection}{Vue de recherche}{46}{section*.8}
\contentsline {section}{\numberline {10.4}Commande dans le tampon agenda}{46}{section.10.4}
\contentsline {section}{\numberline {10.5}Personnalise les vues de l'agenda}{48}{section.10.5}
\contentsline {chapter}{\numberline {11}Marquages pour exportation riche}{49}{chapter.11}
\contentsline {section}{\numberline {11.1}Eléments structurels de marquage}{49}{section.11.1}
\contentsline {section}{\numberline {11.2}Tableaux et images}{51}{section.11.2}
\contentsline {section}{\numberline {11.3}Exemples littéraux}{51}{section.11.3}
\contentsline {section}{\numberline {11.4}Inclusion de fichiers}{52}{section.11.4}
\contentsline {section}{\numberline {11.5}\LaTeX {} embarqué}{52}{section.11.5}
\contentsline {chapter}{\numberline {12}Exportation}{53}{chapter.12}
\contentsline {section}{\numberline {12.1}Options d'exportation}{53}{section.12.1}
\contentsline {section}{\numberline {12.2}Le repartiteur d'exportation}{54}{section.12.2}
\contentsline {section}{\numberline {12.3}Exportation ASCII/Latin-1/UTF-8}{54}{section.12.3}
\contentsline {section}{\numberline {12.4}Exportation HTML}{54}{section.12.4}
\contentsline {section}{\numberline {12.5}Exportation \LaTeX {} et PDF}{55}{section.12.5}
\contentsline {section}{\numberline {12.6}Exportation iCalendar}{55}{section.12.6}
\contentsline {chapter}{\numberline {13}Publication}{57}{chapter.13}
\contentsline {chapter}{\numberline {14}Travail avec le code source}{59}{chapter.14}
\contentsline {chapter}{\numberline {15}Divers}{63}{chapter.15}
\contentsline {section}{\numberline {15.1}Complétion}{63}{section.15.1}
\contentsline {section}{\numberline {15.2}Une vue propre de grande ligne}{63}{section.15.2}
\contentsline {section}{\numberline {15.3}MobileOrg}{64}{section.15.3}
\contentsline {chapter}{\numberline {16}GNU Free Documentation License}{65}{chapter.16}
